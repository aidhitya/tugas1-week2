// Normal

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const firstName = newObject.firstName;
const lastName = newObject.lastName;
const destination = newObject.destination;
const occupation = newObject.occupation;

console.log(firstName, lastName, destination, occupation)


// ES6

const {
    firstName,
    lastName,
    destination,
    occupation,
    spell
} = newObject

console.log(firstName, lastName, destination, occupation, spell)