// Normal

const golden = function goldenFunction() {
    console.log("this is golden!!")
}

golden()

// ES6

const golden = () => console.log("this is golden!!")

golden()